package com.cchallenge.entity;

public class MobileOperator {
	Integer id;
	String name;
	Double rating;

	public MobileOperator(Integer id, String name, Double rating) {
		super();
		this.id = id;
		this.name = name;
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "MobileOperator [id=" + id + ", name=" + name + ", rating=" + rating + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

}
