package com.cchallenge.entity;

public class Person {
	Integer id;
	String name;
	Integer mobileOperator;
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", mobileOperator=" + mobileOperator + "]\n";
	}
	public Person(Integer id, String name, Integer mobileOperator) {
		super();
		this.id = id;
		this.name = name;
		this.mobileOperator = mobileOperator;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMobileOperator() {
		return mobileOperator;
	}
	public void setMobileOperator(Integer mobileOperator) {
		this.mobileOperator = mobileOperator;
	}
	
	@Override
	public int hashCode() {
		
		return this.getId()*5;
	}
	@Override
	public boolean equals(Object obj) {
		if(this.getId()==((Person)obj).id)
		{
			if(this.mobileOperator==((Person)obj).mobileOperator) {
				if(this.name.equals(((Person)obj).name)) {
					return true;
				}
			}
			
		}
			
			
			
		return false;
	}
	
	
	

}
