package com.cchallenge.services;

import java.util.List;
import java.util.Map;

import com.cchallenge.entity.MobileOperator;
import com.cchallenge.entity.Person;
import com.cchallenge.exceptions.InvalidMobileOperatorException;

public interface ChallengeService {

	public List<Person> getPersonsFromTop2Operator(List<Person> personList, List<MobileOperator> mobileOperatorList);
	public Map<Person,MobileOperator> getPersonAndOperatorByPersonId(Integer id, List<Person> personList, List<MobileOperator> mobileOperatorList);
	boolean registerPerson(Person p, List<MobileOperator> mobileOperators, List<Person> personList)
			throws InvalidMobileOperatorException;

	List<Person> getPersonUsingMobileOperator(Integer Mid, List<Person> personList);
}
