package com.cchallenge.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.cchallenge.entity.MobileOperator;
import com.cchallenge.entity.Person;
import com.cchallenge.exceptions.InvalidMobileOperatorException;
import com.cchallenge.services.ChallengeService;

public class ChallengeServiceImpl implements ChallengeService {

	@Override
	public boolean registerPerson(Person p, List<MobileOperator> mobileOperators, List<Person> personList)
			throws InvalidMobileOperatorException {

		int searchFlag = 0;
		for (MobileOperator mo : mobileOperators) {
			if (p.getMobileOperator() == mo.getId()) {
				searchFlag = 1;
				personList.add(p);
				return true;
			}
		}

		if (searchFlag == 0)
			throw new InvalidMobileOperatorException("Invalid Mobile Operator");
		return false;

	}



	@Override
	public List<Person> getPersonUsingMobileOperator(Integer Mid, List<Person> personList) {
		List<Person> returnList = new ArrayList<>();
		for (Person p : personList) {
			if (p.getMobileOperator() == Mid) {
				returnList.add(p);
			}
		}

		// TODO Auto-generated method stub
		return returnList;
	}

	@Override
	public List<Person> getPersonsFromTop2Operator(List<Person> personList, List<MobileOperator> mobileOperatorList) {

		Collections.sort(mobileOperatorList, (a,b) -> {if(a.getRating()<b.getRating())
			return 1;
		else if(a.getRating()>b.getRating())
			return -1;
		return 0;});
		
		//Collections.sort(mobileOperatorList, (a,b) -> b.getRating()-a.getRating());
		
		
		return personList.stream().filter(p -> (p.getMobileOperator()==mobileOperatorList.get(0).getId()|| p.getMobileOperator()==mobileOperatorList.get(1).getId())).collect(Collectors.toList());
		
	}



	@Override
	public Map<Person, MobileOperator> getPersonAndOperatorByPersonId(Integer id, List<Person> personList,
			List<MobileOperator> mobileOperatorList) {
		
		Map<Person,MobileOperator> returningMap = new HashMap<>();
		
		personList.forEach((p)->{
			if(p.getId()==id) {
				mobileOperatorList.forEach((m)->{
					if(p.getMobileOperator()==m.getId()) {
						returningMap.put(p, m);
					}
					
				});
			}
			
		});
		
		
		return returningMap;
	}

}
