package com.cchallenge;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.cchallenge.entity.MobileOperator;
import com.cchallenge.entity.Person;
import com.cchallenge.services.ChallengeService;
import com.cchallenge.services.impl.ChallengeServiceImpl;

public class Driver {

	public static void main(String[] args) {

		List<Person> personList = new ArrayList<>();
		personList.add(new Person(1, "Parth", 1));
		personList.add(new Person(2, "Rama", 2));
		personList.add(new Person(3, "Ankit", 2));
		personList.add(new Person(4, "Bhupendra", 3));
		personList.add(new Person(5, "Kanti", 3));

		List<MobileOperator> mobileOperatorList = new ArrayList<>();
		mobileOperatorList.add(new MobileOperator(1, "BSNL", 3.3));
		mobileOperatorList.add(new MobileOperator(2, "JIO", 3.4));
		mobileOperatorList.add(new MobileOperator(3, "Airtel", 4.3));

		ChallengeService service = new ChallengeServiceImpl();

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter options:(1,2,3,4):");

		try {
			switch (sc.nextInt()) {

			case 1:

				service.registerPerson(new Person(1, "Abhishek", 1), mobileOperatorList, personList);
				System.out.println(personList);
				break;
			case 2:
				Integer mid = 2;
				List<Person> pList = service.getPersonUsingMobileOperator(mid, personList);
				for (Person p : pList)
					System.out.println(p);
				break;
			case 3:
				List<Person> pList2 = service.getPersonsFromTop2Operator(personList, mobileOperatorList);
				for (Person p : pList2)
					System.out.println(p);
				break;
			case 4:
				Integer pid = 4;
				Map<Person, MobileOperator> pairMap =  service.getPersonAndOperatorByPersonId(pid, personList, mobileOperatorList);
				Person findPerson=null;
				for(Person p : personList) {
					if(p.getId()==pid) {
						findPerson =p;
					}
				}
				
				System.out.println(findPerson);
				System.out.println(pairMap.get(new Person(4, "Bhupendra", 3)));
				break;
			default:
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
