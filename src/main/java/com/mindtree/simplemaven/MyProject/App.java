package com.mindtree.simplemaven.MyProject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        createConnection("jdbc:mysql://localhost:3306/mindtree", "root", "login@123");
    }
    public static boolean createConnection(String url,String username, String password) {
    	//System.out.println( "Hello World!" );
        try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(url,username,password);
			if (con != null) {
				System.out.println("connection created"+con);
				return true;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return false;
    }
}
